package ru.renessans.jvschool.volkov.tm.constant;

public interface CmdConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

}