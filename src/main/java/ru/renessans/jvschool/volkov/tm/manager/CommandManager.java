package ru.renessans.jvschool.volkov.tm.manager;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.*;

public class CommandManager {

    private final Map<CommandType, List<Command>> commands = new HashMap<>();

    public CommandManager(final CommandType... commandTypes) {
        if (Objects.isNull(commandTypes))
            return;
        for (CommandType commandType : commandTypes) {
            this.commands.put(commandType, new ArrayList<>());
        }
    }

    public void addCommands(final CommandType commandType, final Command... commands) {
        if (Objects.isNull(commandType) || isNoCommandType(commandType))
            return;
        final List<Command> commandList = this.commands.get(commandType);
        commandList.addAll(Arrays.asList(commands));
    }

    public List<Command> getCommandsList(final CommandType commandType) {
        if (Objects.isNull(commandType) || isNoCommandType(commandType))
            return new ArrayList<>();
        return commands.get(commandType);
    }

    private boolean isNoCommandType(final CommandType commandType) {
        return this.commands.get(commandType) == null;
    }

}