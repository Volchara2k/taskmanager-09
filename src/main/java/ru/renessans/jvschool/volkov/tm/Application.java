package ru.renessans.jvschool.volkov.tm;

import ru.renessans.jvschool.volkov.tm.printer.CommandPrinter;

public class Application {

    public static void main(final String[] args) {
        final CommandPrinter printer = new CommandPrinter();
        printer.print(args);
    }

}