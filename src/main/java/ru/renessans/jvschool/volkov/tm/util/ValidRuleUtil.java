package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.List;

public final class ValidRuleUtil {

    public static boolean isEmptyOrNull(final String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isEmptyOrNull(final String... strings) {
        return strings == null || strings.length < 1;
    }

    public static boolean isEmptyOrNull(final List<Command> commands) {
        return commands == null || commands.isEmpty();
    }

}