package ru.renessans.jvschool.volkov.tm.printer;

import ru.renessans.jvschool.volkov.tm.api.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.constant.NotificationConst;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.repository.CommandRepositoryImpl;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;
import java.util.Scanner;

final public class CommandPrinter implements NotificationConst {

    private final ICommandRepository commandRepository = new CommandRepositoryImpl();

    public void print(final String... args) {
        if (ValidRuleUtil.isEmptyOrNull(args)) {
            commandPrintLoop();
        } else {
            final String arg = args[0];
            System.out.println(notificationByCommandType(CommandType.ARG, arg));
        }
    }

    private void commandPrintLoop() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CmdConst.EXIT.equals(command)) {
            System.out.println((String.format(ENTER_FORMAT_MSG_FACTOR, CmdConst.EXIT, CmdConst.HELP)));
            command = scanner.nextLine();
            System.out.println(notificationByCommandType(CommandType.CMD, command));
        }
    }

    private String notificationByCommandType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) {
            return NO_COMMAND_TYPE_MSG;
        }
        if (ValidRuleUtil.isEmptyOrNull(command))
            return NO_COMMAND_MSG;

        String result = "";
        if (commandType.isArg())
            result = commandRepository.getNotifyByCommandType(CommandType.ARG, command);
        if (commandType.isCmd())
            result = commandRepository.getNotifyByCommandType(CommandType.CMD, command);
        if (ValidRuleUtil.isEmptyOrNull(result))
            return String.format(FORMAT_MSG_UNKNOWN, command);
        return result;
    }

}