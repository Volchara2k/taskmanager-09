package ru.renessans.jvschool.volkov.tm.model;

import ru.renessans.jvschool.volkov.tm.constant.ArgConst;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.constant.DescriptionConst;
import ru.renessans.jvschool.volkov.tm.constant.NotificationConst;
import ru.renessans.jvschool.volkov.tm.util.SystemMonitorUtil;

public enum Command implements NotificationConst {

    VERSION(
            CmdConst.VERSION, ArgConst.VERSION, DescriptionConst.VERSION,
            VERSION_MSG
    ),

    ABOUT(
            CmdConst.ABOUT, ArgConst.ABOUT, DescriptionConst.ABOUT,
            String.format(FORMAT_MSG_ABOUT, DEV_MSG, DEV_MAIL_MSG)
    ),

    INFO(
            CmdConst.INFO, ArgConst.INFO, DescriptionConst.INFO,
            SystemMonitorUtil.getInstance().formatInfo()
    ),

    EXIT(
            CmdConst.EXIT, null, DescriptionConst.EXIT,
            EXIT_MSG
    ),

    HELP(
            CmdConst.HELP, ArgConst.HELP, DescriptionConst.HELP
    ),

    ARGUMENT(
            CmdConst.ARGUMENTS, ArgConst.ARGUMENTS, DescriptionConst.ARGUMENTS
    ),

    COMMAND(
            CmdConst.COMMANDS, ArgConst.COMMANDS, DescriptionConst.COMMANDS
    );

    private String cmd = "";

    private String arg = "";

    private String description = "";

    private String notification = "";

    Command(String cmd, String arg, String description) {
        this.cmd = cmd;
        this.arg = arg;
        this.description = description;
    }

    Command(String cmd, String arg, String description, String notification) {
        this.cmd = cmd;
        this.arg = arg;
        this.description = description;
        this.notification = notification;
    }

    public String getCmd() {
        return this.cmd;
    }

    public void setCmd(final String cmd) {
        this.cmd = cmd;
    }

    public String getArg() {
        return this.arg;
    }

    public void setArg(final String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getNotification() {
        return this.notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.cmd != null && !this.cmd.isEmpty())
            result.append("cmd: ").append(this.cmd);
        if (this.arg != null && !this.arg.isEmpty())
            result.append(", arg: ").append(this.arg);
        if (this.description != null && !this.description.isEmpty())
            result.append("\n\t - ").append(this.description);
        return result.toString();
    }

}