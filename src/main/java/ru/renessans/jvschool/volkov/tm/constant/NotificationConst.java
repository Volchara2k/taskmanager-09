package ru.renessans.jvschool.volkov.tm.constant;

public interface NotificationConst {

    String FORMAT_MSG_UNKNOWN = "Неизвестная команда: %s";

    String FORMAT_MSG_HELP = "%s";

    String FORMAT_MSG_ARG = "%s";

    String FORMAT_MSG_CMD = "%s";

    String FORMAT_MSG_INFO =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    String FORMAT_MSG_ABOUT = "%s - разработчик; \n%s - почта.";

    String ENTER_FORMAT_MSG_FACTOR =
            "\nВведите команду для получения данных о приложении.\n" +
                    "\tВведите команду \"%s\" для выхода из приложения, \"%s\" - для получения справки.\n";

    String VERSION_MSG = "Версия: 1.0.8.";

    String DEV_MSG = "Valery Volkov";

    String DEV_MAIL_MSG = "volkov.valery2013@yandex.ru";

    String EXIT_MSG = "Выход из приложения!";

    String NO_COMMAND_MSG = "Входная команда отсутствует!";

    String NO_COMMAND_TYPE_MSG = "Не указан тип команды!";

}