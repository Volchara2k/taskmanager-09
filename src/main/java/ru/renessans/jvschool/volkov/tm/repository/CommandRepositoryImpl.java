package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.constant.NotificationConst;
import ru.renessans.jvschool.volkov.tm.manager.CommandManager;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

final public class CommandRepositoryImpl implements ICommandRepository, NotificationConst {

    private final List<Command> commands = Arrays.asList(
            Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND, Command.EXIT
    );

    private final CommandManager commandManager;

    public CommandRepositoryImpl() {
        this.commandManager = new CommandManager(CommandType.ARG, CommandType.CMD);
        addArgCommands();
        addCmdCommands();
        setNotifiesForLists();
    }

    @Override
    public String getNotifyByCommandType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType))
            return null;
        if (ValidRuleUtil.isEmptyOrNull(command))
            return null;

        final List<Command> commands = this.commandManager.getCommandsList(commandType);
        if (ValidRuleUtil.isEmptyOrNull(commands))
            return null;

        for (Command commandModel : commands) {
            if (Objects.equals(commandByType(commandType, commandModel), command))
                return commandModel.getNotification();
        }
        return null;
    }

    private String commandByType(final CommandType commandType, final Command command) {
        if (Objects.isNull(commandType) | Objects.isNull(command))
            return null;
        if (commandType.isArg())
            return command.getArg();
        if (commandType.isCmd())
            return command.getCmd();
        return null;
    }

    private void setNotifiesForLists() {
        final AtomicReference<String> results = new AtomicReference<>("");
        this.commands.forEach(command -> {
            results.updateAndGet(v -> v + command.toString() + "\n");
            Command.HELP.setNotification(String.format(FORMAT_MSG_HELP, results.get()));
        });
        final AtomicReference<String> arg = new AtomicReference<>("");
        commandsByType(CommandType.ARG).forEach(command -> {
            arg.updateAndGet(v -> v + command.getArg() + "\n");
            Command.ARGUMENT.setNotification(String.format(FORMAT_MSG_ARG, arg.get()));
        });
        final AtomicReference<String> cmd = new AtomicReference<>("");
        commandsByType(CommandType.CMD).forEach(command -> {
            cmd.updateAndGet(v -> v + command.getCmd() + "\n");
            Command.COMMAND.setNotification(String.format(FORMAT_MSG_CMD, cmd.get()));
        });
    }

    private List<Command> commandsByType(final CommandType commandType) {
        if (Objects.isNull(commandType))
            return new ArrayList<>();
        if (commandType.isArg())
            return this.commandManager.getCommandsList(CommandType.ARG);
        if (commandType.isCmd())
            return this.commandManager.getCommandsList(CommandType.CMD);
        return new ArrayList<>();
    }

    private void addCmdCommands() {
        this.commandManager.addCommands(CommandType.CMD,
                Command.HELP, Command.VERSION, Command.ABOUT,
                Command.INFO, Command.ARGUMENT, Command.COMMAND,
                Command.EXIT
        );
    }

    private void addArgCommands() {
        this.commandManager.addCommands(CommandType.ARG,
                Command.HELP, Command.VERSION, Command.ABOUT,
                Command.INFO, Command.ARGUMENT, Command.COMMAND
        );
    }

}