package ru.renessans.jvschool.volkov.tm.constant;

public interface DescriptionConst {

    String HELP = "вывод списка команд";

    String VERSION = "вывод версии программы";

    String ABOUT = "вывод информации о разработчике";

    String INFO = "вывод информации о системе";

    String ARGUMENTS = "вывод списка поддерживаемых аргументов";

    String COMMANDS = "вывод списка поддерживаемых терминальных команд";

    String EXIT = "закрыть приложение";

}